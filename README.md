# BennuTest

## Cómo probar

```
$ git clone https://gitlab.com/gonzalo.esteban.lagos/bennutest.git
$ cd bennutest/mantenedor
$ mvn clean install -DskipTests
$ docker-compose up
```

```
Acceder al sistema a través de localhost:8087
```

### Mantenedor de colegios
``` Ingresar via http://localhost:8087/ ```
![](mantenedor_colegios.gif)

### Mantenedor de profesores
``` Ingresar via http://localhost:8087/profesores ```
![](mantenedor_profesores.gif)


##### Frameworks utilizados

* [PostgreSQL v10.x](https://www.postgresql.org/) - Motor de Base de datos
* [Spring-data (Hibernate + JPA)](http://spring.io/projects/spring-data-jpa) - Modelo/Entidades
* [SpringBoot v2.1](https://spring.io/projects/spring-boot) - Orquestador MVC
* [Vaadin v10](https://vaadin.com/) - Vista
* [Maven](https://maven.apache.org/) - Gestor de dependencias





