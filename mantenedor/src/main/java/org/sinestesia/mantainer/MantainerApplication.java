package org.sinestesia.mantainer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.vaadin.flow.spring.annotation.EnableVaadin;

@SpringBootApplication
@EnableVaadin
public class MantainerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MantainerApplication.class, args);
	}
}
