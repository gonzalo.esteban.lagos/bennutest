package org.sinestesia.mantainer;

import org.sinestesia.mantainer.model.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepoService {

    private ColegioRepository colegioRepo;
    private AlumnoRepository alumnoRepo;
    private AsignaturaRepository asignaturaRepo;
    private ProfesorRepository profesorRepo;
    private NotaRepository notaRepo;


    public RepoService(@Autowired ColegioRepository colegioRepo,
                       @Autowired AlumnoRepository alumnoRepo,
                       @Autowired AsignaturaRepository asignaturaRepo,
                       @Autowired ProfesorRepository profesorRepo,
                       @Autowired NotaRepository notaRepo) {

        this.colegioRepo = colegioRepo;
        this.alumnoRepo = alumnoRepo;
        this.asignaturaRepo = asignaturaRepo;
        this.profesorRepo = profesorRepo;
        this.notaRepo = notaRepo;
    }

    public ColegioRepository getColegioRepo() {
        return colegioRepo;
    }

    public AlumnoRepository getAlumnoRepo() {
        return alumnoRepo;
    }

    public AsignaturaRepository getAsignaturaRepo() {
        return asignaturaRepo;
    }

    public ProfesorRepository getProfesorRepo() {
        return profesorRepo;
    }

    public NotaRepository getNotaRepo() {
        return notaRepo;
    }
}
