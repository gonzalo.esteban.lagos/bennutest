package org.sinestesia.mantainer.controller;

import org.sinestesia.mantainer.model.entity.Colegio;
import org.sinestesia.mantainer.model.entity.Profesor;
import org.sinestesia.mantainer.model.repository.ColegioRepository;
import org.sinestesia.mantainer.model.repository.ProfesorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WebController {

    @Autowired
    ColegioRepository repo;

    @Autowired
    ProfesorRepository prepo;

    @RequestMapping("/save")
    public String process() {
        // repo.save(new Colegio("Colegio Experimental 3", "X"));

        return "Done";
    }

    @RequestMapping("/findAll")
    public String findAll() {
        String result = "";
        for (Colegio c : repo.findAll()) {
            result += c.toString() + "<br>";
        }

        result += "<br>";
        for (Profesor p : prepo.findAll()) {
            result += p.toString() + "<br>";
        }

        return result;
    }



}
