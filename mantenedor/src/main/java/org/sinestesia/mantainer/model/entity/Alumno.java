package org.sinestesia.mantainer.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Alumno")
public class Alumno implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_alumno")
    private Long id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="fecha_nacimiento")
    private Date fechaNacimiento;

    @OneToMany(mappedBy="alumno", targetEntity=Nota.class)
    private List<Nota> notas;

    @ManyToOne(targetEntity=Colegio.class)
    @JoinColumn(name="colegio_id", referencedColumnName="id_colegio")
    private Colegio colegio;


    protected Alumno() { }

    public Alumno(String nombre, Date fechaNacimiento, Colegio colegio) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.colegio = colegio;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public Colegio getColegio() {
        return colegio;
    }

    public void setColegio(Colegio colegio) {
        this.colegio = colegio;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", notas=" + notas +
                ", colegio=" + colegio +
                '}';
    }
}
