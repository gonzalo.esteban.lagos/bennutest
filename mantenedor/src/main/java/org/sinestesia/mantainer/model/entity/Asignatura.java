package org.sinestesia.mantainer.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Asignatura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_asignatura")
    private Long id;

    @Column(name="nombre")
    private String nombre;

    @OneToMany(mappedBy="asignatura",targetEntity=Profesor.class)
    private List<Profesor> profesores;


    protected Asignatura() { }

    public Asignatura(String nombre, List<Nota> notas, List<Profesor> profesores) {
        this.nombre = nombre;
        this.profesores = profesores;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Profesor> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }



    @Override
    public String toString() {
        return "Asignatura{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", profesores=" + profesores +
                '}';
    }
}
