package org.sinestesia.mantainer.model.entity;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Colegio")
public class Colegio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_colegio")
    private Long id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="direccion")
    private String direccion;


    @OneToMany(mappedBy="colegio", targetEntity=Alumno.class)
    private List<Alumno> alumnos;

    @OneToMany(mappedBy="colegio", targetEntity=Profesor.class)
    private List<Profesor> profesores;


    protected Colegio() { }

    public Colegio(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    @Override
    public String toString() {
        return "Colegio{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
