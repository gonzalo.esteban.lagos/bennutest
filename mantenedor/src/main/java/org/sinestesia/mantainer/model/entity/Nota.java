package org.sinestesia.mantainer.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Nota implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_nota")
    private Long id;

    private Double nota;

    @ManyToOne(targetEntity=Alumno.class)
    @JoinColumn(name="alumno_id", referencedColumnName="id_alumno")
    private Alumno alumno;

    @ManyToOne(targetEntity=Profesor.class)
    @JoinColumn(name="profesor_id", referencedColumnName="id_profesor")
    private Profesor profesor;


    protected Nota() { }

    public Nota(Long id, Double nota, Alumno alumno, Asignatura asignatura) {
        this.nota = nota;
    }

    public Long getId() {
        return id;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    @Override
    public String toString() {
        return "Nota{" +
                "id=" + id +
                ", nota=" + nota +
                ", alumno=" + alumno +
                ", profesor=" + profesor +
                '}';
    }
}
