package org.sinestesia.mantainer.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
public class Profesor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_profesor")
    private Long id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Column(name="activo")
    private Boolean activo;


    @ManyToOne(targetEntity=Colegio.class)
    @JoinColumn(name="colegio_id", referencedColumnName="id_colegio")
    private Colegio colegio;

    @ManyToOne(targetEntity=Asignatura.class)
    @JoinColumn(name="asignatura_id",referencedColumnName="id_asignatura")
    private Asignatura asignatura;

    @OneToMany(mappedBy="profesor", targetEntity=Nota.class)
    private List<Alumno> notas;


    protected Profesor() { }

    public Profesor(String nombre, LocalDate fechaNacimiento, Boolean activo, Colegio colegio, Asignatura asignatura) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.activo = activo;
        this.colegio = colegio;
        this.asignatura = asignatura;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Boolean getActivo() {
        return activo;
    }

    public String getActivoAsString() {
        return (activo) ? "Sí" : "No";
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Colegio getColegio() {
        return colegio;
    }

    public String getNombreColegio() {
        return colegio.getNombre();
    }

    public void setColegio(Colegio colegio) {
        this.colegio = colegio;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public String getNombreAsignatura() {
        return asignatura.getNombre();
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    public List<Alumno> getNotas() {
        return notas;
    }

    public void setNotas(List<Alumno> notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", activo=" + activo + "}";
    }
}
