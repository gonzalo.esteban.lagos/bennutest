package org.sinestesia.mantainer.model.repository;

import org.sinestesia.mantainer.model.entity.Colegio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ColegioRepository extends JpaRepository<Colegio, Long> {

    /*
    @Modifying
    @Query("UPDATE Colegio c SET c.nombre=?2, c.direccion=?3 WHERE c.id=?1 ")
    void update(Long id, String nombre, String direccion);
    */

}
