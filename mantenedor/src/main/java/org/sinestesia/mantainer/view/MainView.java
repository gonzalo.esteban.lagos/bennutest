package org.sinestesia.mantainer.view;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.ThemeDefinition;
import com.vaadin.flow.theme.lumo.Lumo;
import elemental.js.html.JsWindow;
import org.sinestesia.mantainer.RepoService;
import org.sinestesia.mantainer.model.entity.Colegio;
import org.sinestesia.mantainer.model.repository.ColegioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.flow.component.formlayout.FormLayout;



@Route
@EnableJpaRepositories
@Theme(value = Lumo.class, variant = Lumo.DARK)
@PageTitle("Mantenedor de Colegios")
@StyleSheet(value="style.css")
public class MainView extends VerticalLayout {

    private RepoService repoService;
    private ColegioRepository repo;

    // Componentes
    H3 titulo = new H3("Mantenedor de Colegios");
    TextField nombre = new TextField("Nombre");
    TextField direccion = new TextField("Dirección");
    Button agregar = new Button("Agregar", VaadinIcon.PLUS.create());
    Button limpiar = new Button("Limpiar", VaadinIcon.MINUS.create());
    Button actualizar = new Button("Guardar", VaadinIcon.EDIT.create());
    // Tabla
    Grid<Colegio> grid = new Grid<>();



    // Layouts
    HorizontalLayout botones = new HorizontalLayout();
    FormLayout form = new FormLayout();


    public MainView(ColegioRepository repo) {
        // Repositorio JPA
        this.repo = repo;
        // Configurar componentes visuales (formulario)
        setup();

    }

    private void listItems() {
        grid.setItems(repo.findAll());
    }

    private void setup() {
        // Estilo
        this.setSpacing(true);
        this.setPadding(true);
        this.getStyle().set("padding", "20px");

        nombre.setPlaceholder("Nombre");
        nombre.setMinLength(1);
        nombre.setMaxLength(30);
        nombre.setSizeFull();


        direccion.setPlaceholder("Dirección");
        direccion.setMinLength(1);
        direccion.setMaxLength(50);
        direccion.setSizeFull();

        agregar.addClickListener(e -> insert() );
        limpiar.addClickListener(e -> clear() );

        limpiar.setEnabled(false);

        agregar.setSizeFull();
        limpiar.setSizeFull();

        nombre.addValueChangeListener(textfield -> observeLimpiar());
        nombre.addKeyUpListener(textfield -> observeLimpiar());
        direccion.addValueChangeListener(textfield -> observeLimpiar());
        direccion.addKeyUpListener(textfield -> observeLimpiar());

        botones.add(agregar);
        botones.add(limpiar);

        botones.setSizeFull();
        botones.setAlignItems(Alignment.CENTER);


        form.setWidth("600px");
        VerticalLayout vl = new VerticalLayout(titulo, nombre, direccion, botones);
        form.add(vl);
        add(form);

        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.setHeightByRows(true);
        grid.getStyle().set("margin", "5px");
        grid.addColumn(Colegio::getId).setHeader("ID").setFlexGrow(0).setResizable(true);
        grid.addColumn(Colegio::getNombre).setHeader("Nombre").setFlexGrow(1).setResizable(true);
        grid.addColumn(Colegio::getDireccion).setHeader("Dirección").setFlexGrow(1).setResizable(true);

        grid.addComponentColumn(colegio -> {
            Button editBtn = new Button("Editar", VaadinIcon.EDIT.create());
            editBtn.addClickListener(event -> edit(colegio));
            return editBtn;
        }).setFlexGrow(0);

        grid.addComponentColumn(colegio -> {
            Button deleteBtn = new Button("Borrar", VaadinIcon.TRASH.create());
            deleteBtn.addClickListener(event -> delete(colegio));
            return deleteBtn;
        }).setFlexGrow(0);

        add(grid);
        refresh();
    }

    private void clear() {
        nombre.clear();
        direccion.clear();
    }

    private void update(Colegio c, String nuevoNombre, String nuevoDireccion) {
        c.setNombre(nuevoNombre);
        c.setDireccion(nuevoDireccion);
        repo.save(c);
    }

    private void insert() {
        if (!nombre.isEmpty() && !direccion.isEmpty()) {
            String nombreColegio = nombre.getValue();
            String direccionColegio = direccion.getValue();
            Colegio colegio = new Colegio(nombreColegio, direccionColegio);
            repo.save(colegio);

            Notification.show("Colegio " + colegio.getNombre() + " ingresado");
        } else {
            Notification.show("Para ingresar un nuevo colegio, ingresa su nombre y su dirección");
        }
        listItems();
        clear();
    }

    private void observeLimpiar() {
        boolean isNombreEmpty = (nombre.isEmpty());
        boolean isDireccionEmpty = (direccion.isEmpty());
        if (isNombreEmpty && isDireccionEmpty)
            limpiar.setEnabled(false);
        else
            limpiar.setEnabled(true);
    }

    private void delete(Colegio colegio) {
        repo.delete(colegio);
        Notification.show("Colegio " + colegio.getNombre() + " removido");
        refresh();
    }

    private void edit(Colegio colegio) {

        TextField textNombreDialog = new TextField("Nombre");
        TextField textDireccionDialog = new TextField("Dirección");
        Button btnActualizar = new Button("Guardar cambios", VaadinIcon.REFRESH.create());
        Button btnCancelar = new Button("Salir", VaadinIcon.CLOSE.create());
        HorizontalLayout botonesLayout = new HorizontalLayout(btnActualizar, btnCancelar);
        VerticalLayout layout = new VerticalLayout(textNombreDialog,
                textDireccionDialog,
                botonesLayout);
        FormLayout formDialog = new FormLayout(layout);
        Dialog dialog = new Dialog(formDialog);
        dialog.setCloseOnEsc(true);

        textNombreDialog.setPlaceholder(colegio.getNombre());
        textDireccionDialog.setPlaceholder(colegio.getDireccion());

        btnActualizar.addClickListener(e -> {
            String nuevoNombre = (textNombreDialog.isEmpty()) ?  colegio.getNombre() :  textNombreDialog.getValue();
            String nuevoDireccion = (textDireccionDialog.isEmpty()) ? colegio.getDireccion() : textDireccionDialog.getValue();
            update(colegio, nuevoNombre, nuevoDireccion);
            dialog.close();
            Notification.show("Colegio '" + colegio.getNombre() + "' modificado.");
            listItems();
        });

        btnCancelar.addClickListener(e -> dialog.close());

        dialog.open();
        refresh();
    }

    private void refresh() {
        listItems();
    }




}
