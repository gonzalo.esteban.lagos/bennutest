package org.sinestesia.mantainer.view;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.ThemeDefinition;
import com.vaadin.flow.theme.lumo.Lumo;
import elemental.js.html.JsWindow;
import org.hibernate.Hibernate;
import org.sinestesia.mantainer.RepoService;
import org.sinestesia.mantainer.model.entity.Asignatura;
import org.sinestesia.mantainer.model.entity.Colegio;
import org.sinestesia.mantainer.model.entity.Profesor;
import org.sinestesia.mantainer.model.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.vaadin.flow.component.formlayout.FormLayout;

import java.util.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Locale;


@Route(value="profesores")
@EnableJpaRepositories
@Theme(value = Lumo.class, variant = Lumo.DARK)
@PageTitle("Mantenedor de profesores")
public class MantenedorProfesores extends VerticalLayout {

    private RepoService repos;

    private ColegioRepository colegioRepo;
    private AlumnoRepository alumnoRepo;
    private AsignaturaRepository asignaturaRepo;
    private ProfesorRepository profesorRepo;
    private NotaRepository notaRepo;

    // Componentes

    private H3 titulo = new H3("Mantenedor");
    private H3 subtitulo = new H3("Profesores");

    private TextField textNombre = new TextField();
    private DatePicker datepickerFechaNacimiento = new DatePicker();
    private ComboBox<Colegio> comboboxColegio = new ComboBox<>();
    private ComboBox<Asignatura> comboboxAsignatura = new ComboBox<>();
    private Checkbox checkboxActivo = new Checkbox("Activo");

    private Button btnAgregar = new Button("Agregar", VaadinIcon.PLUS.create());
    private Button btnLimpiar = new Button("Limpiar", VaadinIcon.MINUS.create());
    private Button btnActualizar = new Button(VaadinIcon.REFRESH.create());
    // Tabla
    private Grid<Profesor> grid = new Grid<>();








    public MantenedorProfesores(RepoService repos) {
        // Repositorios JPA
        this.repos = repos;
        this.profesorRepo = repos.getProfesorRepo();
        this.alumnoRepo = repos.getAlumnoRepo();
        this.asignaturaRepo = repos.getAsignaturaRepo();
        this.notaRepo = repos.getNotaRepo();
        this.colegioRepo = repos.getColegioRepo();

        setupUI();      // Configurar componentes visuales
        setupEvents();  // Establecer eventos
        loadData();     // Cargar datos
        refresh();      // Refrescar
    }



    private void setupUI() {

        setJustifyContentMode(JustifyContentMode.EVENLY);
        // Layouts
        HorizontalLayout titulos = new HorizontalLayout();
        VerticalLayout formLayout = new VerticalLayout();
        FormLayout form = new FormLayout();
        HorizontalLayout botones = new HorizontalLayout();

        textNombre.setSizeFull();
        datepickerFechaNacimiento.setSizeFull();
        comboboxColegio.setSizeFull();
        comboboxAsignatura.setSizeFull();

        textNombre.setRequired(true);
        textNombre.setAutofocus(true);
        textNombre.setRequiredIndicatorVisible(true);
        datepickerFechaNacimiento.setRequired(true);
        comboboxColegio.setRequired(true);
        comboboxAsignatura.setRequired(true);

        textNombre.setMaxLength(30);
        textNombre.setMinLength(1);
        // datepickerFechaNacimiento.setLocale(Locale.forLanguageTag("es"));
        comboboxColegio.setPreventInvalidInput(true);
        comboboxAsignatura.setPreventInvalidInput(true);

        btnLimpiar.setEnabled(false);

        // Formulario
        form.setWidth("600px");
        form.addFormItem(textNombre, "Nombre");
        form.addFormItem(datepickerFechaNacimiento, "Fecha de nacimiento");
        form.addFormItem(comboboxColegio, "Colegio");
        form.addFormItem(comboboxAsignatura, "Asignatura");
        form.addFormItem(checkboxActivo, "");

        titulos.add(titulo, subtitulo);
        botones.add(btnAgregar, btnLimpiar, btnActualizar);
        // Tabla de datos
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.setHeightByRows(true);
        grid.addColumn(Profesor::getId).setHeader("ID").setFlexGrow(0);
        grid.addColumn(Profesor::getNombre).setHeader("Nombre").setFlexGrow(1).setResizable(true);
        grid.addColumn(Profesor::getFechaNacimiento).setHeader("Fecha de nacimieto").setFlexGrow(1).setResizable(true);
        grid.addColumn(Profesor::getActivoAsString).setHeader("Activo").setResizable(true);
        grid.addColumn(Profesor::getNombreAsignatura).setHeader("Asignatura").setResizable(true);
        grid.addColumn(Profesor::getNombreColegio).setHeader("Colegio").setResizable(true);
        grid.addComponentColumn(profesor -> {
            Button btnEditar = new Button("Editar", VaadinIcon.EDIT.create());
            btnEditar.addClickListener(e -> edit(profesor));
            return btnEditar;
        }).setFlexGrow(1);
        grid.addComponentColumn(profesor -> {
            Button btnEliminar = new Button("Eliminar", VaadinIcon.MINUS_CIRCLE.create());
            btnEliminar.addClickListener(e -> delete(profesor));
            return btnEliminar;
        }).setFlexGrow(1);

        // Agregar los componentes visuales a la vista
        this.add(titulos);
        this.add(form);
        this.add(botones);
        this.add(grid);
    }

    private void setupEvents() {
        // Eventos de botones
        btnActualizar.addClickListener(e -> refresh());
        btnAgregar.addClickListener(e -> insert());
        btnLimpiar.addClickListener(e -> clear());
        // ValueChangeListener para limpiar
        textNombre.addValueChangeListener(component -> observeLimpiar());
        textNombre.addKeyUpListener(component -> observeLimpiar());
        datepickerFechaNacimiento.addValueChangeListener(component -> observeLimpiar());
        comboboxAsignatura.addValueChangeListener(component -> observeLimpiar());
        comboboxColegio.addValueChangeListener(component -> observeLimpiar());
        checkboxActivo.addValueChangeListener(component -> observeLimpiar());
    }

    private void loadData() {

        comboboxColegio.setItemLabelGenerator(Colegio::getNombre);
        comboboxColegio.setItems(colegioRepo.findAll());

        comboboxAsignatura.setItemLabelGenerator(Asignatura::getNombre);
        comboboxAsignatura.setItems(asignaturaRepo.findAll());

    }

    private void insert() {

        if (!checkEmpty()) {
            String pNombre = textNombre.getValue();
            LocalDate pFechaNacimiento = datepickerFechaNacimiento.getValue();
            Boolean pActivo = checkboxActivo.getValue();
            Colegio pColegio = comboboxColegio.getValue();
            Asignatura pAsignatura = comboboxAsignatura.getValue();

            Profesor p = new Profesor(pNombre, pFechaNacimiento, pActivo, pColegio, pAsignatura);
            profesorRepo.save(p);

            refresh();
            Notification.show("Profesor '" + pNombre + "' agregado.");
            clear();
        } else {
            Notification.show("Todos los campos deben estar llenos", 5000, Notification.Position.MIDDLE);
            textNombre.focus();
        }
    }

    private void edit(Profesor profesor) {

        TextField textNombreDialog = new TextField();
        DatePicker datepickerFechaNacimientoDialog = new DatePicker();
        ComboBox<Colegio> comboboxColegioDialog = new ComboBox<>();
        ComboBox<Asignatura> comboboxAsignaturaDialog = new ComboBox<>();
        Checkbox checkboxActivoDialog = new Checkbox("Activo");
        Button btnGuardarDialog = new Button("Guardar cambios", VaadinIcon.REFRESH.create());
        Button btnCancelarDialog = new Button("Salir", VaadinIcon.CLOSE.create());
        HorizontalLayout botonesLayout = new HorizontalLayout(btnGuardarDialog, btnCancelarDialog);
        VerticalLayout layout = new VerticalLayout(textNombreDialog, datepickerFechaNacimientoDialog,
                comboboxColegioDialog, comboboxAsignaturaDialog, checkboxActivoDialog, botonesLayout);
        FormLayout formDialog = new FormLayout(layout);

        Dialog dialog = new Dialog(formDialog);
        dialog.setCloseOnEsc(true);

        comboboxColegioDialog.setItemLabelGenerator(Colegio::getNombre);
        comboboxColegioDialog.setItems(colegioRepo.findAll());
        comboboxAsignaturaDialog.setItemLabelGenerator(Asignatura::getNombre);
        comboboxAsignaturaDialog.setItems(asignaturaRepo.findAll());

        textNombreDialog.setValue(profesor.getNombre());
        datepickerFechaNacimientoDialog.setValue(profesor.getFechaNacimiento());
        comboboxColegioDialog.setPlaceholder(profesor.getNombreColegio());
        comboboxAsignaturaDialog.setPlaceholder(profesor.getNombreAsignatura());
        checkboxActivoDialog.setValue(profesor.getActivo());

        textNombreDialog.setMaxLength(30);
        textNombreDialog.setAutofocus(true);

        btnGuardarDialog.addClickListener(e -> {

            String nuevoNombre =
                    (textNombreDialog.isEmpty()) ?  profesor.getNombre() :  textNombreDialog.getValue();

            LocalDate nuevoFechaNac =
                    (datepickerFechaNacimientoDialog.isEmpty()) ?
                    profesor.getFechaNacimiento() : datepickerFechaNacimientoDialog.getValue();

            Colegio nuevoColegio =
                    (comboboxColegioDialog.isEmpty()) ? profesor.getColegio() : comboboxColegioDialog.getValue();

            Asignatura nuevoAsignatura =
                    (comboboxAsignaturaDialog.isEmpty()) ? profesor.getAsignatura() : comboboxAsignaturaDialog.getValue();

            Boolean nuevoActivo = checkboxActivoDialog.getValue();

            update(profesor, nuevoNombre, nuevoFechaNac, nuevoColegio, nuevoAsignatura, nuevoActivo);
            dialog.close();
            Notification.show("Colegio '" + profesor.getNombre() + "' modificado.");
            refresh();
        });

        btnCancelarDialog.addClickListener(e -> dialog.close());

        dialog.open();

    }

    private void delete(Profesor profesor) {
        profesorRepo.delete(profesor);
        Notification.show("Profesor '" + profesor.getNombre() + "' eliminado");
        refresh();
    }

    private boolean checkEmpty() {
        boolean emptiness =
                textNombre.isEmpty() || datepickerFechaNacimiento.isEmpty() ||
                comboboxAsignatura.isEmpty() || comboboxColegio.isEmpty();

        if (textNombre.isEmpty())
            textNombre.setRequiredIndicatorVisible(true);

        if (datepickerFechaNacimiento.isEmpty())
            datepickerFechaNacimiento.setInvalid(true);

        if (comboboxColegio.isEmpty())
            comboboxColegio.setInvalid(true);

        if (comboboxAsignatura.isEmpty())
            comboboxAsignatura.setInvalid(true);

        return emptiness;
    }

    private void observeLimpiar() {
        if (checkEmpty())
            btnLimpiar.setEnabled(false);
        else
            btnLimpiar.setEnabled(true);
    }

    private void listItems() {
        grid.setItems(profesorRepo.findAll());
    }

    private void refresh() {
        listItems();
        Notification.show("Profesores: " + profesorRepo.count() + " items");
    }

    private void clear() {
        textNombre.clear();
        textNombre.clear();
        datepickerFechaNacimiento.clear();
        comboboxAsignatura.clear();
        comboboxColegio.clear();
        checkboxActivo.clear();
    }

    private void update(Profesor profesor, String nuevoNombre, LocalDate nuevoFechaNac,
                        Colegio nuevoColegio, Asignatura nuevoAsignatura, Boolean nuevoActivo) {

        profesor.setNombre(nuevoNombre);
        profesor.setFechaNacimiento(nuevoFechaNac);
        profesor.setColegio(nuevoColegio);
        profesor.setAsignatura(nuevoAsignatura);
        profesor.setActivo(nuevoActivo);
        profesorRepo.save(profesor);
        Notification.show("Item modificado");
    }

    private void setFormEnabled(boolean enabled) {
        textNombre.setEnabled(enabled);
        datepickerFechaNacimiento.setEnabled(enabled);
        comboboxColegio.setEnabled(enabled);
        comboboxAsignatura.setEnabled(enabled);
        checkboxActivo.setEnabled(enabled);
    }


}
