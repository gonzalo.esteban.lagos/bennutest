

-- SELECTS --
-- Pregunta 1: Consulta que obtenga todos los profesores de un colegio ordenados por nombre --
SELECT Profesor.nombre as Profesor, Asignatura.nombre as Asignatura
FROM (Profesor
INNER JOIN Asignatura ON Profesor.asignatura_id = Asignatura.id_asignatura)
WHERE Profesor.colegio_id = 1
ORDER BY Profesor.nombre;

-- Pregunta 2: Consulta que obtenga todos los alumnos con promedio rojo por asignatura --
SELECT Alumno.nombre as Alumno, Nota.nota as Nota, Asignatura.nombre as Asignatura
FROM (((Alumno
INNER JOIN Nota ON Alumno.id_alumno = Nota.alumno_id)
INNER JOIN Profesor ON Nota.profesor_id = Profesor.id_profesor)
INNER JOIN Asignatura ON Profesor.asignatura_id = Asignatura.id_asignatura)
WHERE Nota.nota < 4.0 AND Alumno.colegio_id = 1 AND Profesor.colegio_id = 1
GROUP BY Asignatura.nombre, Alumno.nombre, Nota.nota;

-- Pregunta 3: Consulta que contenga el alumno con mejor nota por asignatura --
SELECT DISTINCT ON (Asignatura.id_asignatura) Asignatura.id_asignatura, Alumno.nombre as Alumno, Nota.nota as Nota, Asignatura.nombre as Asignatura
FROM (((Asignatura
INNER JOIN Profesor ON Asignatura.id_asignatura = Profesor.asignatura_id)
INNER JOIN Nota ON Profesor.id_profesor = Nota.profesor_id )
INNER JOIN Alumno ON Nota.alumno_id = Alumno.id_alumno)
WHERE Alumno.colegio_id = 1 AND Profesor.colegio_id = 1
ORDER BY Asignatura.id_asignatura, Nota.nota DESC;


