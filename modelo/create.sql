

-- -----------------------------------------------------
-- Tabla Colegio
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Colegio (
  id_colegio SERIAL PRIMARY KEY,
  nombre     VARCHAR(30) NOT NULL,
  direccion  VARCHAR(45) NOT NULL  
);
-- -----------------------------------------------------
-- Tabla Asignatura
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Asignatura (
  id_asignatura SERIAL PRIMARY KEY,
  nombre        VARCHAR(30) NOT NULL
);
-- -----------------------------------------------------
-- Tabla Profesor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Profesor (
  id_profesor      SERIAL PRIMARY KEY,
  nombre           VARCHAR(30) NOT NULL,
  fecha_nacimiento DATE        NOT NULL,
  activo           BOOLEAN     NOT NULL,
  colegio_id       INT         NOT NULL,
  asignatura_id    INT         NOT NULL,
  FOREIGN KEY (colegio_id) REFERENCES Colegio (id_colegio) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (asignatura_id) REFERENCES Asignatura (id_asignatura) ON DELETE CASCADE ON UPDATE CASCADE
);
-- -----------------------------------------------------
-- Tabla Alumno
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Alumno (
  id_alumno        SERIAL PRIMARY KEY,
  nombre           VARCHAR(30) NOT NULL,
  fecha_nacimiento DATE        NOT NULL,
  colegio_id       INT         NOT NULL,
  FOREIGN KEY (colegio_id) REFERENCES Colegio (id_colegio) ON DELETE CASCADE ON UPDATE CASCADE
);
-- -----------------------------------------------------
-- Tabla Nota
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Nota (
  id_nota       SERIAL PRIMARY KEY,
  profesor_id 	INT    NOT NULL,
  alumno_id     INT    NOT NULL,
  nota          DECIMAL NOT NULL,
  FOREIGN KEY (profesor_id) REFERENCES Profesor (id_profesor) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (alumno_id) REFERENCES Alumno (id_alumno) ON DELETE CASCADE ON UPDATE CASCADE
);
