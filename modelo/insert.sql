
-- INSERTS --

-- Colegio --
INSERT INTO Colegio (id_colegio, nombre, direccion) VALUES (1, 'Colegio Experimental X1', 'Calle Experimental #456');

-- Alumno --
INSERT INTO Alumno (id_alumno, nombre, fecha_nacimiento, colegio_id) VALUES (1, 'Juan Perez', '2005-05-31', 1);
INSERT INTO Alumno (id_alumno, nombre, fecha_nacimiento, colegio_id) VALUES (2, 'Cesar Leon', '2005-02-14', 1);
INSERT INTO Alumno (id_alumno, nombre, fecha_nacimiento, colegio_id) VALUES (3, 'Hernan Gonzalez', '2005-08-27', 1);

-- Asignatura --
INSERT INTO Asignatura (id_asignatura, nombre) VALUES (1, 'Historia');
INSERT INTO Asignatura (id_asignatura, nombre) VALUES (2, 'Matematicas');
INSERT INTO Asignatura (id_asignatura, nombre) VALUES (3, 'Lenguaje');

-- Profesor
INSERT INTO Profesor (id_profesor, nombre, fecha_nacimiento, activo, colegio_id, asignatura_id) VALUES (1, 'Julio Cortazar', '1960-07-19', true, 1, 3);
INSERT INTO Profesor (id_profesor, nombre, fecha_nacimiento, activo, colegio_id, asignatura_id) VALUES (2, 'Albert Einstein', '1955-01-13', true, 1, 2);
INSERT INTO Profesor (id_profesor, nombre, fecha_nacimiento, activo, colegio_id, asignatura_id) VALUES (3, 'Isaac Newton', '1970-09-20', true, 1, 1);

-- Nota --
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (1, 3, 1, 5);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (2, 2, 1, 2.3);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (3, 1, 1, 6.7);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (4, 3, 2, 3.1);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (5, 2, 2, 6.5);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (6, 1, 2, 3.8);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (7, 2, 3, 6.1);
INSERT INTO Nota (id_nota, profesor_id, alumno_id, nota) VALUES (8, 3, 3, 6.8);
